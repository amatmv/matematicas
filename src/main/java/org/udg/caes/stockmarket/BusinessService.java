package org.udg.caes.stockmarket;

import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.PersistenceService;
import org.udg.caes.stockmarket.exceptions.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains the business logic of the app.
 * It resembles a EJB from Java EE 7
 */
public class BusinessService {
    PersistenceService mPS;
    BrokerService mBS;

    @Inject
    public BusinessService(PersistenceService ps, BrokerService bs) {
        mPS = ps;
        mBS = bs;
    }

    private Integer orderId = 0;

    public User getUser(String id) throws EntityNotFound {
        return mPS.getUser(id);
    }

    public Portfolio getPortfolio(String userId, String name) throws EntityNotFound, ElementNotExists {
        User u = mPS.getUser(userId);
        return u.getPortfolio(name);
    }

    public void saveUser(User u) {
        mPS.saveUser(u);
    }

    public void saveGroup(UserGroup g) {
        mPS.saveUserGroup(g);
    }

    public void savePortfolio(Portfolio p) throws ElementNotExists, EntityNotFound, ElementAlreadyExists, InvalidOperation {
        mPS.savePortfolio(p);
    }

    public Boolean hasStock(String userId, String ticker) throws EntityNotFound {
        User u = mPS.getUser(userId);
        for (Portfolio p : u.getAllPortfolios()) {
            if (p.hasStock(ticker))
                return true;
        }
        return false;
    }

    // Computes the real-time value of the users assets
    public Double getUserValuation(String userId) throws Exception {
        User u = mPS.getUser(userId);
        double v = 0.0;
        for (Portfolio p : u.getAllPortfolios()) {
            v += this.getPortfolioValuation(p);
        }

        return v;
    }

    // Computes the real-time value of the group assets
    public Double getUserGroupValuation(String userId) throws Exception {
        UserGroup ug = mPS.getUserGroup(userId);

        double v = 0.0;
        for (User u : ug.getUsers())
            v += this.getUserValuation(u.getId());

        return v;
    }

    // Finds the portfolio with the best real-time valuation
    public Portfolio getBestPortfolio(String userId) throws EntityNotFound, StockNotFound {
        User u = mPS.getUser(userId);
        double max = Double.MIN_VALUE;
        Portfolio result = null;
        for (Portfolio p : u.getAllPortfolios()) {
            double profit = this.getPortfolioStockValue(p);

            if (profit > max) {
                max = profit;
                result = p;
            }
        }
        return result;
    }

    // Finds the portfolio with the best real-time valuation
    public Portfolio getBestGroupPortfolio(String groupId, Portfolio pBest, double value) throws EntityNotFound, StockNotFound {
        UserGroup ug = mPS.getUserGroup(groupId);
        double max = value;

        Portfolio p;
        for (User u : ug.getUsers()) {
            p = this.getBestPortfolio(u.getId());
            double profit = this.getPortfolioStockValue(p);

            if (profit > max) {
                max = profit;
                pBest = p;
            }
        }

        return pBest;
    }

    public float getPortfolioStockValue(Portfolio p) throws StockNotFound {
        float profit = 0;

        for (Stock s : p)
            profit += s.getQuantity() * (mBS.getPrice(s.getTicker()) - s.getBuyPrice());
        for (Gold g : p.getGold())
            profit += g.getmAmountOz() * (mBS.getPrice("GOLD") - g.getValuation());
        for (Silver s : p.getSilver())
            profit += s.getmAmountOz() * (mBS.getPrice("SILVER") - s.getValuation());
        return profit;
    }

    public float getPortfolioValuation(Portfolio p) throws StockNotFound {
        float value = 0;

        for (Stock s : p)
            value += s.getQuantity() * mBS.getPrice(s.getTicker());
        for (Gold g : p.getGold())
            value += g.getmAmountOz() * mBS.getPrice("GOLD");
        for (Silver s : p.getSilver())
            value += s.getmAmountOz() * mBS.getPrice("SILVER");

        return value;
    }


    public List<String> getCommonStocks(String userId1, String userId2) throws EntityNotFound {
        User u1 = mPS.getUser(userId1);
        User u2 = mPS.getUser(userId2);
        ArrayList<String> stocks = new ArrayList<String>();
        for (Portfolio p : u1.getAllPortfolios()) {
            for (Stock s : p)
                if (!stocks.contains(s.getTicker()) && u2.hasStock(s.getTicker()))
                    stocks.add(s.getTicker());
        }
        return stocks;
    }

    /**
     * This method sends an Order for a give User
     *
     * @param o Order
     * @throws OrderNotSent
     */
    public void placeOrder(Order o) throws NoSuchMethodException {
        orderId++;
        o.setId(orderId.toString());
        o.setStatus(Order.PROCESSING);
        mPS.saveOrder(o);
        o.send(mBS);
    }
}
