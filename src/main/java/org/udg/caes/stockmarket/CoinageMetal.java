package org.udg.caes.stockmarket;

public abstract class CoinageMetal {

    double mAmountOz;
    double mPriceOz;

    public CoinageMetal(double mAmountOz, double mPriceOz) {
        this.mAmountOz = mAmountOz;
        this.mPriceOz = mPriceOz;
    }

    public double getmAmountOz() {
        return mAmountOz;
    }

    public void setmAmountOz(double mAmountOz) {
        this.mAmountOz = mAmountOz;
    }

    public double getmPriceOz() {
        return mPriceOz;
    }

    public void setmPriceOz(double mPriceOz) {
        this.mPriceOz = mPriceOz;
    }

    double getValuation() { return mAmountOz * mPriceOz; }

}
