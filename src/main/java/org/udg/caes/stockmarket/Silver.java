package org.udg.caes.stockmarket;

/**
 * Created by imartin on 14/12/16.
 */
public class Silver extends CoinageMetal {

    public Silver(double mAmountOz, double mPriceOz) {
        super(mAmountOz,mPriceOz);
    }

}
