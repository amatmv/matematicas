package org.udg.caes.stockmarket;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import org.udg.caes.stockmarket.EXTERNAL.BrokerService;
import org.udg.caes.stockmarket.EXTERNAL.StockMarket;
import org.udg.caes.stockmarket.FAKE.Fake_PS_MySQL;
import org.udg.caes.stockmarket.exceptions.*;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class UtilsService {

    BrokerService mBS;
    EventBus mEB;
    

    @Inject
    public UtilsService(BrokerService bs, EventBus eb) {
        mBS = bs;
        mEB = eb;
        mEB.register(this);
    }

    public ArrayList<String> searchStock(Predicate<String> comparingFunc){
        ArrayList<String> r = new ArrayList<String>();

        for (StockMarket sm: mBS.getMarkets())
            for (String s : sm.getAllStocks())
                if (comparingFunc.test(s))
                    r.add(s);
        return r;
    }

    public ArrayList<String> searchStocks(String substring) {
        return this.searchStock(stock -> stock.contains(substring));
    }

    public List<String> searchStocksWithValue(double price) throws BrokerInternalError {
        return this.searchStock(stock -> {
            try {
                return stockHasBiggerPrice(stock, price);
            } catch (BrokerInternalError brokerInternalError) {
                brokerInternalError.printStackTrace();
                return false;
            }
        });
    }

    public boolean stockHasBiggerPrice(String stock, double price) throws BrokerInternalError
    {
        try{
            return mBS.getPrice(stock) > price;
        }
        catch (StockNotFound stockNotFound) {
            throw new BrokerInternalError();
        }
    }

    @Subscribe public void processOrderStatusChange(OrderStatusEvent event) throws EntityNotFound, ElementNotExists, InvalidOperation {
        Fake_PS_MySQL ps = new Fake_PS_MySQL();
        Order order = event.getOrder();
        if (order.getStatus() == Order.PROCESSING) {
            ps.saveOrder(order);
        } else if (orderIsCompleted(event)) {
            Portfolio portfolio = order.getPortfolio();
            updatePortfolio(portfolio,order);
            ps.saveOrder(order);
            ps.savePortfolio(portfolio);
        }
    }

    public Boolean orderIsCompleted(OrderStatusEvent event)
    {
        Order order=event.getOrder();
        return  event.wasInProcessing() && (order.isCompleted() || order.isPartiallyCompleted());
    }

    public void updatePortfolio(Portfolio portfolio, Order order)
    {
        int orderType=order.getOrderType();
        if (orderType == Order.STOCK)
            portfolio.addStock(new Stock(order.getTicker(), order.getEfQuant(), order.getEfPrice()));
        if (orderType == Order.GOLD)
            portfolio.getGold().add(new Gold(order.getEfQuant(), order.getEfPrice()));
        if (orderType == Order.SILVER)
            portfolio.getSilver().add(new Silver(order.getEfQuant(), order.getEfPrice()));
    }

}
