package org.udg.caes.stockmarket;

/**
 * Created by imartin on 14/12/16.
 */
public class Gold extends CoinageMetal {

    public Gold(double mAmountOz, double mPriceOz) {
        super(mAmountOz,mPriceOz);
    }

}
